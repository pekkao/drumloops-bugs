import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-credits',
  templateUrl: './credits.page.html',
  styleUrls: ['./credits.page.scss'],
})
export class CreditsPage implements OnInit {

  constructor(public router: Router, 
    public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  goToHomePage() {
    this.router.navigateByUrl('home');
  }

}

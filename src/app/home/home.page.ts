import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  // Drum loops array will have titles of drum loops. Correspondent images
  // and audio files have the same name
  private drumLoops = [];

  constructor(public router: Router) {}

  ngOnInit() {
    this.drumLoops = ['Simple', 'Groovy', 'Mambo', 'Funk', 'Rock'];
  }

  private goToMusicPage(audio: string) {
    this.router.navigateByUrl('music' + audio);
  }

}
